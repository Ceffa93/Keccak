\clearpage
\section{Known attacks and weaknesses}\label{attacks}
{\normalsize

Keccak is a very wide family of sponge functions, and as such there is a great number of possible attacks.
Most of them exploit weaknesses in the sponge function, rather then attacking directly the core algorithm, Keccak-$f$.
Others are directed to non-standard Keccak variants, to possible implementations of the algorithm or even to versions of Keccak not used for hashing (\cite{8} and \cite{12}).
However, since our main interest was the security of Keccak-$f$, we chose to focus on the standard Keccak versions approved by NIST, because their sponge construction is very simple, and for short-length inputs they just consist in a single Keccak-$f$ iteration.

There are three main kinds of attacks that can be applied to Keccak, and to any other hash algorithm as well:
\begin{center}
\begin{description}
\item[$\cdot$] 
\textsl{Preimage attack}:\\* given an output $o$, find an input $i$ so that $o = h(i)$.
\item[$\cdot$] 
\textsl{Second preimage attack}:\\* given an input $i$, find an input $i'$ so that $h(i) = h(i')$.
\item[$\cdot$] 
\textsl{Collision attack}:\\* find two inputs $i$ and $i'$ so that $h(i) = h(i')$.
\end{description}
\end{center}
Keccak is very resistant to first and second preimage attacks.
Most of the work on these kinds of attacks has been done by Pawell Morawiecki (\cite{5}, \cite{6} and \cite{9}).
Only two rounds have been practically attacked in preimage attacks, and none in second preimage attacks, however Morawiecki proposed non-practical attacks up to nine rounds that succeed in reducing the total complexity of the algorithm.
\begin{figure}[ht!]
\centering
\def\svgwidth{200pt} 
\input{rounds.pdf_tex}
\caption{Number of rounds reached in collision attacks. }\label{fig:rounds}
\end{figure}
}

Figure \ref{fig:rounds} reports the number of rounds reached in practical collision attacks in the four standard Keccak variants.
These results were possible thanks to the work of Itai Dinur, Orr Dunkelman, and Adi Shamir, who described the attacks used to obtain them in \cite{2} and \cite{7}.

The algorithm is still very secure at the time of this writing, since each of these four versions uses Keccak-$f$[1600] as its core function, whose number of rounds is 24.
There are also attacks for a greater number of rounds, which are not-practical, but succeed in reducing the total security of the algorithm.
None of them has ever reached the full 24 rounds Keccak-$f$[1600] though, and not even the 12 rounds employed by Keccak-$f$[25].

This work was mainly focused on collision attacks, and we manage to find actual collisions for Keccak-512 reduced to three rounds with the implementation of the attack described in \cite{2}.

There are also many studies that test the security of Keccak from a more theoretical point of view without attacking it directly, but showing potential weaknesses of the algorithm that could be exploited by future attacks. Such is the case of the work of Joan Daemen and Gilles Van Assche, who studied the differential weaknesses of Keccak in \cite{3}.

Section \ref{trails} describes the differential trails proposed in \cite{3} more in depth.

Section \ref{subset_known} reports the subset attack proposed by Itai Dinur, Orr Dunkelman, and Adi Shamir in \cite{2}.\\*

}
\subsection{Differential trails}\label{trails}
\normalsize{
Differential cryptanalysis is a branch of cryptography that studies how differences in the input of a cypher can influence its output.
If an algorithm has a predictable difference evolution, it can be subject to several attacks.
DES, for example, was definitely broken and declared insecure due to chosen plaintext attacks, which exploit the differential weakness of the algorithm.
To analyse the differential complexity of Keccak it is useful to calculate the lowest-weight differential trails.
Joan Daemen and Gilles Van Assche wrote the most important paper on Keccak's trails (\cite{3}), and most of the concepts described in this section are based on their work.

For the implementation of trails generation algorithms made in this work, refer to section \ref{mytrails}.\\*
}

\subsubsection{\normalsize{Trail definition}} 
\normalsize{
A trail is a differential path across a certain number of rounds.
The difference $d$ and the number of rounds $n$ must be predefined for each trail.
$m1$ and $m2$ are two plaintexts with difference $d$, while the Keccak-$f$ algorithm is here denoted by $h$. 
$p$ is the probability that the following expression is true:
\begin{equation}\label{eq:trail_property}
h( m1 ) \oplus h( m2 ) = h (m1 \oplus m2)
\end{equation}
Finally, the weight $w$ of a trail is:
\begin{equation}\label{eq:weight}
w = log_2\ p
\end{equation}
A trail of difference $d$ across $n$ rounds has weight $w$ if the probability of finding two messages $m1$ and $m2$ with difference $d$, which satisfy the formula, is $1/2^w$.

The resistance of the algorithm to differential attacks, reduced to $n$ rounds, is directly connected to the lowest weight trail for $n$ rounds.
Therefore the goal is to find the difference(s) $d$  that gives the lowest-weight to the trail(s) for $n$ rounds.\\*

}
\subsubsection{\normalsize{Intro to trails generation}} 
\normalsize{
All Keccak-$f$ functions, except for $\chi$, are linear steps.\\*
This imply the following:
\begin{equation}\label{eq:linear_property}
s( m1 ) \oplus s( m2 ) = s (m1 \oplus m2)
\end{equation}\*\\*
This means that there is no difference between:
\begin{description}
\item[$\cdot$] Pass each input to $s$, and then get their difference;
\item[$\cdot$] Pass the difference of the inputs to $s$;
\end{description}
Since this is always true for linear functions, the probability $p$ of Equation \ref{eq:trail_property} is 1 (Equation \ref{eq:trail_property} and \ref{eq:linear_property} are exactly the same), and the weight of the step is zero.
Therefore no weight is ever added to the trails due to the linear steps, the only function that can reduce the probability is $\chi$, because to a single difference input is associated an affine subspace of outputs.
\begin{figure}[h!]
\centering
\includegraphics[width=0.7\textwidth]{graphic/affine.png}
\caption{$\chi$ affine subspace of output differeces, with respective weight, reverse weight and Hamming weight. Source: \cite{3}. \label{fig:affine}}
\end{figure}

In Figure \ref{fig:affine}, for each input difference are reported the bases of its output subspace, and an offset value.
For example, for input 00011, the output values are all the solutions of the following formula, with different values of the $a_x$ coefficients:
\begin{center}
$ output = a_1 * 00010 + a_2 * 00100 + a_3 * 01000 + 00001$ 
\end{center}
An output space with $n$ bases can generate $2^n$ outputs (including cyclic shifts of the 5 bits).
Each output subspace include a value identical to the input difference, for example, for an input of 11111:
\begin{center}
$ 11111 = 0 * 00011 + 1 * 00110 + 0 * 01100 + 1* 11000 +  00001$ 
\end{center}
This means that $\chi$, for a space with $n$ bases, has $1/2^n$ probability of acting as an identity function, and therefore its weight is $n$.
When a trail reach $\chi$, it treats it as an identity step, leaving the current difference untouched, but adding to the total weight the weight of each row.

Since 00000 has weight 0, it is important to reach the $\chi$ round with very few active bits, in order to minimize the weight of the trail.
In order to achieve this, it is mandatory to also consider the linear steps in the trails generation, because, while they don't directly add weight, they can drastically modify the current difference state.
If the new state has a lot of active bits, a lot of weight will be added when the $\chi$ step is reached, and the final trail will not be useful.
The behaviour of the linear steps is the following:
\begin{description}
\item[$\cdot$]$\iota$ does not modify the difference in any way, so it can be ignored.
\item[$\cdot$]$\rho$ shifts the difference bits along the $z$ axis, but does not modify their number.
\item[$\cdot$]$\pi$ switches the bit positions along the $x$ and $y$ axis, without adding extra active bits.
\item[$\cdot$]$\theta$ is the only linear step that modifies the number of active bits (usually adding them, for an input with few active bits), and it does so if some of the state columns have an odd number of bits.\\*If all of them have an even number of bits, the state is called CP-Kernel, and $\theta$ act as identity, which is the ideal case.
\end{description}
A few active bits and maintaining the CP-Kernel are the basis of all the techniques to generate trails described in the next section.

Before proceeding with the generation of trails, it is useful to define the propagation weight, reverse propagation weight and Hamming weight of a state:
\begin{description}
\item[$\cdot$] The propagation weight describes the probability to generate an output row given a certain input row, and it is just another name for the weight of a trail defined above.
\item[$\cdot$] The reverse propagation weight describes the probability that an output row has been generated by a certain input row.
\item[$\cdot$] The Hamming weight is the number of active bits in a row.
\end{description}
The only weight used for three-rounds trails generation is the propagation weight, however the Hamming weight is always a good way to measure the quality of a difference state, and the reverse propagation weight is used to extend a trail to more rounds with a process called backward trail extension (\cite{3}).
The values of the weights for each input row are reported in Figure \ref{fig:affine}.\\*
}

\subsubsection{\normalsize{Trails generation}} 
\normalsize{
Keccak-$f$ executes the five round functions in the following order:
\begin{equation}\label{eq:round_functions}
\theta \rightarrow \rho \rightarrow \pi \rightarrow \chi \rightarrow \iota
\end{equation}
While building a trail, it is possible to remove the linear steps before $\chi$ in the first round, because they could eventually be reconstructed backwards.
By calling $\lambda$ the linear steps before $\chi$ and removing $\iota$, it is possible to write the path of a trail as follows:
\begin{equation}\label{eq:trail_steps}
\chi_0 \rightarrow \lambda_1 \rightarrow \chi_1 \rightarrow \lambda_2 \rightarrow ...  \rightarrow \lambda_n \rightarrow \chi_n
\end{equation}

To generate the lowest-weight trails for one round, which consists of a single call to $\chi$, it is possible to place one bit in any position.\ $\chi$ will add 2 to the weight, as evident from the table shown in Figure \ref{fig:affine}.

To generate the lowest-weight trail for two rounds, two active bits must be placed in a single column.\ $\chi$ will immediately add 4 to the weight (2+2).\ $\theta$ will pass as an identity function, because the bits were evenly placed in a single column, then $\rho$ and $\pi$ will move those bits to different positions, and finally $\chi$ will add weight again, 2 for each line with an active bit, 4 in total.
The lowest-weight for a two-rounds trail is then 4+4 = 8.

Extending the trails to an extra round is quite problematic: it is not possible to use a single column of bits anymore, because at the end of the second round the state is no more in the CP-Kernel, and the lowest weight trail found in this way, by Duc et al., is of weight 4+4+24 = 32.
To maintain the CP-Kernel for another round, vortices must be used.
A vortex is basically a set of n columns (from 2 to 5) of two bits each.
The second bit of each column must have the same $y$ of the first bit of the next column, forming a sort of chain.

This is shown more clearly in Figure \ref{fig:vortex}.
\begin{figure}[h!]
\centering
\def\svgwidth{0.6\textwidth} 
\input{vortex.pdf_tex}
\caption{Structure of a 6-bits vortex}\label{fig:vortex}
\end{figure}
\*\\*Having more than one column adds an interesting property to the trail: it is possible that, for some input differences, $\rho$ and $\pi$ will move the active bits to new positions, so that they will form new columns, maintaining the CP-Kernel.
For a 6-bits vortex, the initial weight is 12 (2 multiplied by 6 bits), and, if the CP-Kernel is mainteined for all three rounds, a weight of $12 * 3 = 36$ is to be expected.
It is indeed possible to obtain trails of weight 36 using 6-bits vortices.
4-bits vortices would be a better choice, because the weight of the trail, in the optimal case, would be $8 * 3 = 24$.
However they are very few in number compared to the 6-bits vortices, and none of them leads to a trail with a weight lower than 36.

A trail of weight 36 is actually worse than the trail of weight 32 found with the naive method, but it is immediate to see that the trend of the trail generated with the vortices is more regular.
For a number of rounds greater than three, the second approach will be the only feasible option.
Joan Daemen and Gilles Van Assche were able to find a trail of weight 74 for six rounds, which is just a bit more then 12 for round. This result would not be possible without the use of vortices.

The problem with differential trails is that it is not clear how to use them in a differential attack and I've not found practical attacks based directly on them.
However the subset attack to Keccak-512, which will be described in Section \ref{subset_known}, exploits an internal characteristic, which is similar to a trail, but takes into account differences between parts of a single input state.
}
\*\\*
\subsection{Subset attack} \label{subset_known}
\normalsize{
This section reports the attack for Keccak-512 reduced to three rounds proposed in \cite{2} by Dinur, Dunkelman and Shamir.
First is described the general idea of the attack, then the choice of the characteristic and finally the time and memory complexity of the whole attack.

For the practical implementation of the attack refer to \ref{mysubset}. \\*
\subsubsection{\normalsize{The general idea}} 
\normalsize{
The subset attack is based on the birthday paradox, which states that, given a space of values of size $n$, $n/2$ items must be considered to have a probability of 50\% that two of them will have the same value. Therefore the subset attack goal is to reduce the space in which all the outputs of Keccak will surely be included.
Then it is sufficient to try more than $\sqrt{n}$ inputs, and a collision should be found.
As shown in Figure \ref{fig:subsetspace}, the problem is that the inputs cannot be randomly chosen, or the output space will immediately grow so large, that even its root square would be a number too big to make the attack practically feasible.
The solution to this is to carefully chose the inputs so that they will follow an internal characteristic, which is very similar to the trails explained in Section \ref{trails}.\\*
\begin{figure}[h!]
\centering
\def\svgwidth{.5\textwidth} 
\input{subsetspace.pdf_tex}
\caption{Inputs and outputs space in a subset attack}\label{fig:subsetspace}
\end{figure}

\subsubsection{\normalsize{Internal characteristic}} 
\normalsize{

While in trails generation the difference is between two inputs, in a characteristic the difference is between groups of slices of the input matrix.
The parameter $i$ defines how many slices are in a single group.

For example in Figure \ref{fig:subsetinput} there are 4 blocks of slices, so $i=4$, and because $w=16$, there are 4 blocks with 4 slices each.
The difference is a three-dimensional matrix of the same size of the state, where each bit is 0 if the correspondent bit is equal to the other $w/i$ ones, 1 otherwise.
As in trails generation, a good initial difference must be found in order to lead to a low-weight characteristic.
Since the differences are internal in a characteristic, a matrix full of zeroes is also a possible difference, and this happens when the blocks are identical.
This particular case is shown in Figure \ref{fig:subsetinput} (right): the state repeats itself every $i$ bits along the $z$ axis, because the difference between the blocks is initially zero for each bit, so the characteristic has no active bits (left). \\*
\begin{figure}[h!]
\centering
\def\svgwidth{.85\textwidth} 
\input{subsetinput.pdf_tex}
\caption{An example of input (right) with zero-characteristic (left)}\label{fig:subsetinput}
\end{figure}

The goal of this first part of the attack is to generate a trail 1.5 rounds long (stopping just before the $\chi$ of the second round).
A longer trail would have a weight so high that the subset attack would not be practical.

The main reason why it is not possible to generate a characteristic as long as the trails described in Section \ref{trails} is $\iota$.
This function was irrelevant in classic trails, but now the addition of a constant to the first lane introduces active bits in the differences between blocks, and makes it impossible to maintain the CP-Kernel as long as before.

Using the characteristic described above leads to 11 active bits after 1.5 rounds with probability equals to 1.
This means that an input with that internal difference leads to an output space of size 1 after 1.5 rounds.
We also tried to generate characteristics with other differences, for examples with a column of active bits as in classic trails generation, but the best choice seems to be the zero-characteristic described above by a wide margin.
This characteristic is reported in Section \ref{trailsresults}.\\*



}
\subsubsection{\normalsize{Extending the characteristic}} 
\normalsize{
As explained in the previous section, the characteristic cannot go further than 1.5 rounds.
This is immediately evident from the 11 active bits, which would lead to a very high weight for longer trails.
From there, the first step is going over $\chi$, which will add all the weight of the characteristic's final state.
Since there are 11 active bits, in different rows, 22 weight is added, which means that the output space could contain $2^{22}$ different outputs.

Then it is possible to ignore the linear steps of the third round, but to pass the final $\chi$ the only thing to do is to consider all the possible values of the S-boxes that will generate the hash.
Since the hash is 512 bit long, the number of lanes in which it will be contained is $512/64=8$. Therefore the number of S-boxes involved is $i \times 2$, because the hash is included in the first two rows ($5 \leq 8 < 10$), and the block has width $i$.
Each one of these S-boxes has 5 bits as input and as output, so the number of possible values for each one of them is $2^5$.
Therefore the total number of possible inputs to the final $\chi$, to represent a 512 bit hash is:
\begin{equation}\label{eq:ni}
n_i = 2^{10 \times i}
\end{equation}
Since each input to the $\chi_2$ step is actually one of the $2^{22}$ possible values contained in the output space of $\chi_1$, the number of inputs to $\chi_2$ must be multiplied by that number.
This is schematically represented by the following graph:
\begin{equation}\label{eq:subset_weight_graph}
\lambda _0 \xrightarrow{2^0} \chi _0 \xrightarrow{2^0} \lambda _1
\xrightarrow{2^0}\chi _1 \xrightarrow{2^{22}} \lambda _2 
\xrightarrow{2^{22}}\chi _2 \xrightarrow{2 ^ {22 + 10 \times i}}
\end{equation} 
and formalized by the following formula:
\begin{equation}\label{eq:subset_weight}
\text{size} = 2^{22} \times n_i = 2^{22 + (10 \times i)} 
\end{equation}
This is the final size of the subset, so after $\sqrt{\text{size}}$ inputs, due to the birthday paradox, a collision should be found.

Clearly a lower value of $i$ leads to a smaller subset, making the attack faster, but a value of $i$ too low may lead to a number of inputs not high enough to reach the square root of the output subset.
Therefore the ideal value of $i$ is the smaller one satisfying the following condition:
\begin{equation}\label{eq:choice_of_i_text}
 \text{number of inputs} > \sqrt{|\text{outputs subspace}|}
\end{equation}
Keccak-512 has a bitrate $r$ of 576 bits, so the number of lanes involved is $576/64$ = 9.
Considering that the last lane has 2 bit of padding in it, then the number of inputs is:
\begin{equation}\label{eq:ni_512}
2^{8*i + (i/2)}
\end{equation}
The formula to find the correct value of $i$ becomes:
\begin{equation}\label{eq:choice_of_i}
2^{8*i + (i/2)} > \sqrt{2^{22 + (10 \times i)}}
\end{equation}
The smaller value that satisfy the condition is $i=4$, because the subset contains $2^{62}$ values, and $2^{34}$ inputs are sufficient to cover $2^{31}$ outputs.

With these values it is possible to proceed to the implementation of the attack, but before moving forward it is important to see how this kind of attacks behaves with the four-rounds Keccak-512 version.
The subset attack is very good for the three-rounds Keccak-512, but adding a single round will introduce all the weight of each single S-box of the $\chi_2$ step.
This is more clear from the following scheme:

\large{
\begin{description}
\item[$\cdot$]$\lambda _0 \xrightarrow{2^0} \chi _0 \xrightarrow{2^0}$
\item[$\cdot$]$ \lambda _1
\xrightarrow{2^0}\chi _1 \xrightarrow{2^{22}} $
\item[$\cdot$]$\lambda _2 
\xrightarrow{2^{22}}\chi _2 \xrightarrow{2 ^ {22 + 25 \times i}}
$
\item[$\cdot$]$\lambda _3
\xrightarrow{2 ^ {22 + 25 \times i}}\chi _3 \xrightarrow{2 ^ {22 + (25 + 10) \times i}}$ 
\end{description}
}\normalsize{This is not feasible for a reasonable number of inputs, the only alternative is to find a low weight characteristic for 2.5 rounds, and then move forward as usual:
}
\large{
\begin{description}
\item[$\cdot$]$\lambda _0 \xrightarrow{2^0} \chi _0 \xrightarrow{2^{?_1}}$
\item[$\cdot$]$ \lambda _1
\xrightarrow{2^{?_1}}\chi _1 \xrightarrow{2^{?_1 + ?_2}} $
\item[$\cdot$]$\lambda _2 
\xrightarrow{2^{?_1 + ?_2}}\chi _2 \xrightarrow{2^{?_1 + ?_2 + ?_3}}
$
\item[$\cdot$]$\lambda _3
\xrightarrow{2^{?_1 + ?_2 + ?_3}}\chi _3 \xrightarrow{2^{?_1 + ?_2 + ?_3 + (10 \times i)}}$ 
\end{description}
}
\normalsize{

Unfortunately trying to find a 2.5 rounds characteristic with a low weight is not an easy task, and we weren't even able to find one with a weight smaller than 100.
This does not imply that these trails don't exists, but there is not a way to find them yet.

This concludes the theory behind the subset attack. Section \ref{mysubset} describes its implementation, and the strategies we adopted to make it practical on an average desktop PC.
}