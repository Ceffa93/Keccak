#include "keccak.h"
#include "ui_keccak.h"


extern "C" {

#include "MainKeccak.h"
}


Keccak::Keccak(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Keccak)
{
    ui->setupUi(this);

    initialize(0);
    ui->asciiRadio->setChecked(true);
    ui->shakeLength->setValidator( new QIntValidator(128, 16777216, this) );
    ui->shakeLength->setText("4288");
    setupFiles();
    print_all_hashes(ui->shakeLength->text().toUInt());
    loadOutputFiles();
    ui->tabContainer->setCurrentIndex(0);


}

Keccak::~Keccak()
{
    inputFile.close();
    u_int8_t i;
    for(i=0;i<11;i++)
        outputFiles[i].close();
    delete ui;
}


void Keccak::setupFiles(){
    inputFile.setFileName("./input.txt");
    outputFiles[0].setFileName("./Output/sha3_224.txt");
    outputFiles[1].setFileName("./Output/sha3_256.txt");
    outputFiles[2].setFileName("./Output/sha3_384.txt");
    outputFiles[3].setFileName("./Output/sha3_512.txt");
    outputFiles[4].setFileName("./Output/keccak_224.txt");
    outputFiles[5].setFileName("./Output/keccak_256.txt");
    outputFiles[6].setFileName("./Output/keccak_384.txt");
    outputFiles[7].setFileName("./Output/keccak_512.txt");
    outputFiles[8].setFileName("./Output/shake_128.txt");
    outputFiles[9].setFileName("./Output/shake_256.txt");
    outputFiles[10].setFileName("./Output/general.txt");

    inputFile.open(QIODevice::ReadWrite);
    QTextStream in(&inputFile);
    QString line = in.readAll();
    ui->inputText->setPlainText(line);
    inputFile.close();

}

void Keccak::loadOutputFiles()
{

    loadFile(0,ui->sha3224Text);
    loadFile(1,ui->sha3256Text);
    loadFile(2,ui->sha3384Text);
    loadFile(3,ui->sha3512Text);
    loadFile(4,ui->keccak224Text);
    loadFile(5,ui->keccak256Text);
    loadFile(6,ui->keccak384Text);
    loadFile(7,ui->keccak512Text);
    loadFile(8,ui->shake128Text);
    loadFile(9,ui->shake256Text);
    loadFile(10,ui->generalText);

}

void Keccak::loadFile(u_int8_t  i, QTextEdit* t){
    outputFiles[i].open(QIODevice::ReadOnly);
    QTextStream in(&outputFiles[i]);
    QString line = in.readAll();
    t->setPlainText(line);
    outputFiles[i].close();
}



void Keccak::on_generateButton_clicked()
{
    write_on_input();
    print_all_hashes(ui->shakeLength->text().toUInt());
    loadOutputFiles();
}

void Keccak::write_on_input(){
    inputFile.open(QIODevice::ReadWrite);
    inputFile.resize(0);
    inputFile.seek(0);
    inputFile.write(ui->inputText->toPlainText().toLocal8Bit());
    inputFile.close();
}

void Keccak::on_binaryRadio_clicked()
{
    initialize(1);
}

void Keccak::on_asciiRadio_clicked()
{
    initialize(0);
}
