#include <stdlib.h>
#include <stdint.h>
#include <stdint.h>
#include "Utils.h"
#include "Generic.h"



extern uint8_t rho_rotations[5][5];


uint64_t ** theta(uint64_t** from, uint8_t width);
uint64_t ** rho(uint64_t** from, uint8_t width);
uint64_t ** pi(uint64_t** from);
uint64_t ** chi(uint64_t** from);
uint64_t ** iota(uint64_t** from,uint64_t* rc, uint8_t current_round);




void theta_fast(uint64_t** from,uint64_t** to, uint8_t width);
void rho_fast(uint64_t** from,uint64_t** to,uint8_t width);
void pi_fast(uint64_t** from,uint64_t** to);
void chi_fast(uint64_t** from,uint64_t** to);
void iota_fast(uint64_t** from,uint64_t rc);
