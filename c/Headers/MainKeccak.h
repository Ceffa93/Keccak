#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include "Sponge.h"

void initialize(uint8_t type);

//Take input from stdin, print the hash for each keccak version
void print_all_hashes(uint32_t shake_length);

//Sha3 versions
uint64_t*  sha3_224();
uint64_t*  sha3_256();
uint64_t*  sha3_384();
uint64_t*  sha3_512();

//Keccak versions
uint64_t*  keccak_224();
uint64_t*  keccak_256();
uint64_t*  keccak_384();
uint64_t*  keccak_512();

//Shake versions
uint64_t*  shake_128(uint32_t shake_length);
uint64_t*  shake_256(uint32_t shake_length);