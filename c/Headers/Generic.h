#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "Utils.h"


uint64_t** alloc_cube();
uint64_t** wipe_cube(uint64_t** cube);
void free_cube(uint64_t** cube);
void random_populate_cube(uint64_t** cube);
void print_cube(uint64_t** cube);
uint64_t** alloc_and_populate();
//ruota una lane di dimensione arbitraria.
uint64_t rot(uint64_t lane, uint8_t offset, uint8_t lane_size);
void fprintf_cube(FILE* f,uint64_t** cube,char* mex,uint8_t tab);
void print_binary_cube(uint64_t** cube,uint8_t l);
uint64_t** get_diff_cube(uint64_t** a,uint64_t**b);
uint8_t compare_cubes(uint64_t** a,uint64_t**b);
void copy_cube(uint64_t** from, uint64_t** to);
void print_extended_hash(uint64_t* hash, int l);

