#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "Generic.h"
#include "Keccak_f.h"

void initialize_sponge(uint8_t type);
void setup_sponge(uint16_t b,uint16_t r,uint32_t hash_length,uint8_t sha_pad);
uint64_t* sponge();