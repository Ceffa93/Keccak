#include "Generic.h"

uint64_t** alloc_cube(){
	uint64_t** cube = calloc(5,sizeof (*cube));
	uint8_t i;
	for(i=0;i<5;i++){
		cube[i] = calloc (5,64);
	}
	return cube;
}

uint64_t** wipe_cube(uint64_t** cube){
	
	uint8_t x,y;
	for (x=0;x<5;x++){
		for (y=0;y<5;y++){
			cube[x][y] = 0;
		}
	}
	return cube;
	
}



void free_cube(uint64_t** cube){
	
	uint8_t x,y;
	for(x=0;x<5;x++){
		free(cube[x]);
	}
	free(cube);
	
}

void random_populate_cube(uint64_t** cube){
	//solo per testare e provare
	uint8_t x,y;
	for (x=0;x<5;x++){
		for (y=0;y<5;y++){
			cube[x][y] = (x*5+y) * 2342342;
		}
	}
	printf("\n\n");

}


void print_binary_cube(uint64_t** cube,uint8_t l){
	uint8_t x,y;
	
		for (y=0;y<5;y++){
			for (x=0;x<5;x++){
			print_binary(cube[x][y], l);
			printf(" ");
		}
		printf("\n");
	}
	printf("\n");

}

void print_extended_hash(uint64_t* hash, int l){
    uint8_t i;
    for (i=0; i<l;i++){
        printf ("%016lx-",hash[i]);
    }
    printf ("%016lx\n",hash[i]);
}
void print_hex_cube(uint64_t** cube){
	uint8_t x,y;
		for (y=0;y<5;y++){
				for (x=0;x<5;x++){

			
			printf("%016lx ",cube[x][y]);
		}
		printf("\n");
	}
				printf("\n");

}



void fprintf_cube(FILE* f,uint64_t** cube,char* mex,uint8_t tab){
	uint8_t x,y;
	if (tab) fprintf(f, "\t\t");
	fprintf(f, "%s",mex );
	for (y=0;y<5;y++){
		if (tab) fprintf(f, "\t\t");
		for (x=0;x<5;x++){
			fprintf(f,"%016lx  ",cube[x][y]);
		}
		fprintf(f,"\n");
	}
		fprintf(f,"\n\n\n");

}

uint64_t** alloc_and_populate(){
	uint64_t** cube = alloc_cube();
	random_populate_cube(cube);
	return cube;	
}

uint64_t rot(uint64_t lane, uint8_t offset, uint8_t lane_size){
	uint64_t res = (lane<<offset | lane>>lane_size-offset);
	if (lane_size!=64) 
		res &= ((uint64_t)1<<lane_size)-1;

	return res;
}

uint64_t** get_diff_cube(uint64_t** a,uint64_t**b){
	uint64_t** ret;
	ret = alloc_cube(ret);
	uint8_t i,j;
	for (i=0;i<5;i++){
		for(j=0;j<5;j++){
			ret[i][j]=b[i][j]^a[i][j];
		}
	}
	return ret;
}


uint8_t compare_cubes(uint64_t** a,uint64_t**b){
	uint8_t i,j;
	for (i=0;i<5;i++){
		for(j=0;j<5;j++){
			if (a[i][j]!=b[i][j])
				return 0;
		}
	}
	return 1;
}

void copy_cube(uint64_t** from, uint64_t** to){
	uint8_t i,j;
	for (i=0;i<5;i++){
		for(j=0;j<5;j++){
			to[i][j]=from[i][j];
		}
	}
}

