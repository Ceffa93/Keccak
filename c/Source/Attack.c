#include "Attack.h"
#include "string.h"
#include "Generic.h"
#include "keccak_f_functions.h"
#include "xxhash.h"
#include <omp.h>


 //hash table sparsehash

uint64_t pows[8]={(uint64_t)1<<4,(uint64_t)1<<8,(uint64_t)1<<12,(uint64_t)1<<16,(uint64_t)1<<20,
	(uint64_t)1<<24,(uint64_t)1<<28,(uint64_t)1<<32};

//Pass 0-7 as argument to choose which part of the attack to execute
void keccak_512_three_rounds_attack(uint8_t fraction){
	
	//Define or initialize variables
	uint64_t i;
	uint64_t cnt=0;
	uint64_t cnt2=0;
	uint64_t next_step = 0;
	uint64_t** cube;
	uint64_t** cube2;
	uint64_t* hash_key;

	//Chose output file name
	char path[5];
	path[0]=fraction+48;
	path[1]='.';
	path[2]='t';
	path[3]='x';
	path[4]='t';

	//Open output file
	FILE* file = NULL;
	file = fopen(path,"a");
	if (file==NULL){
		printf("\nFailed to open file.\n");
		return;
	}

	//Initialize hash table
	struct HashTable* ht = AllocateHashTable(64, 0); 

	//Preallocate cache array
	uint64_t* buckets = calloc((uint64_t)536870912,sizeof(uint64_t));

	if(buckets==NULL)
	{
			fprintf(file,"Fatal: Out of memory.\n");
			return;
	}
	
	//Start loop with omp directives to parallelize the code
	#pragma omp parallel private (cube,cube2,hash_key) 
	{
		//Preallocate memory for each execution thread
		cube = alloc_cube();
		cube2 = alloc_cube();
		hash_key = malloc(64);

		//Main loop
		#pragma omp for
		for(i=0;i<17179869184;i++){

			//Insert a new input
			insert_cube(i,cube);

			//Calculate its output after three rounds
			three_rounds(cube,cube2,64);
			
			//Output reduction technique: keep only outputs that end
			//with the three bit defined by the function argument
			if ((cube[0][0]&0x7) != fraction)
			{
				 #pragma omp critical
		   		{
		   			//Increment counter to keep track of the status of
		   			//the attack. Just an useful debugging information
					cnt++;
				}
				continue;
			}
			
			//Extract hash from the output
			get_hash_from_cube(hash_key,cube);
			
			//Hash the hash with the second hash function.
			//This gives an index shorter than the whole hash 
			uint64_t xxhash = XXH64 (hash_key, 64,0);
			
			//From the index we can get the position inside the cache array,
			//and the position inside the single 64-bit word.
	        uint64_t bucket_index = (xxhash&0x00000007ffffffc0)>>6;

	   		uint64_t bit_index = (uint64_t)1<<(uint8_t)(xxhash&0x3F);

	   		#pragma omp critical
	   		{
	   			//Print status of the attack.
	   			//Just for debugging
	   			if (++cnt>next_step){
	   				
	   				fprintf(file,"[%ld%%] ",cnt/171798691 );
	   				fflush(file);
	   				next_step+=171798691;
	   			}

		   		//If the bit is free, set the bit
		   		if ((buckets[bucket_index]&bit_index)==0)
		   		{
		       		buckets[bucket_index]|=bit_index;
				} 
				//If the bit is not 0, insert the input/output key 
				//in the hash table. Instead of the whole input is stored
				//the index used to generate it.
				else{
					uint64_t* copied_hash = duplicate_hash(hash_key,7);
					HashInsert(ht, PTR_KEY(ht, copied_hash), i);
		        }
	  		}
		}
		//Free thread memory
		free_cube(cube);
		free_cube(cube2);
		free(hash_key);
	}

	//End of phase one.
	//Free the cache array, no longer useful
	fprintf(file,"\nFreeing buckets...\n");
	free (buckets);

	//Start step two
	fprintf(file,"Searching for collisions...\n");
	cnt=0;
	HTItem* item = NULL;

	//Second parallel loop
	#pragma omp parallel  private (cube,cube2,item,hash_key) 
	{
		//Preallocate memory used by each thread
		cube = alloc_cube();
		cube2 = alloc_cube();
		hash_key = malloc(64);

		#pragma omp for
		for(i=0;i<17179869184;i++){ 
	
			//Insert new input
			insert_cube(i,cube);

			//Calculate its output
			three_rounds(cube,cube2,64);

			//Output selection technique: check if the outputs
			//ends with the chosen three bits
			if ((cube[0][0]&0x7) != fraction)
				continue;

			//Get hash from the output matrix
			get_hash_from_cube(hash_key,cube);
			
			//Query hash table for the output 
			item = HashFind(ht, PTR_KEY(ht, hash_key));

			//If an item is found, and the inputs are different, print collision
			if(item!=NULL && item->data!=i)
			{
				fprintf(file,"[%ld,%ld]\n",item->data,i);
			}
		}

		//Free thread memory
		free_cube(cube);
		free_cube(cube2);
		free(hash_key);
		
	}

	//Clear and free hash table
	ClearHashTable(ht);
 	FreeHashTable(ht);
}




void keccak_challenge_400(){
	FILE* file = NULL;
	char* path = "400.txt";

	
	struct HashTable* ht = AllocateHashTable(80, 0); 
	
	uint64_t i;
	
	file = fopen(path,"a");
	if (file==NULL){
		printf("\nFailed to open file.\n");
		return;
	}

	printf("\nStarting Attack.\n\n");
	uint64_t cnt=0;
	uint64_t cnt2=0;
	uint64_t next_step = 0;
	uint64_t** cube;
	uint64_t** cube2;
	uint64_t* hash_key;
	uint64_t* buckets = calloc((uint64_t)536870912,sizeof(uint64_t));

	if(buckets==NULL){
			fprintf(file,"Fatal: Out of memory.\n");
			return;
		}
	
	#pragma omp parallel private (cube,cube2,hash_key) 
	{
		cube = alloc_cube();
		cube2 = alloc_cube();
		hash_key = malloc(80);

		#pragma omp for
		for(i=0;i<1073741824;i++){

			
			insert_cube_challenge_400(i,cube);

			three_rounds(cube,cube2,16);
		

			get_400_hash_from_cube(hash_key,cube);
			
			
			uint64_t xxhash = XXH64 (hash_key, 80,0);
			
			
	        uint64_t bucket_index = (xxhash&0x00000007ffffffc0)>>6;
	   		uint64_t bit_index = (uint64_t)1<<(uint8_t)(xxhash&0x3F);


	   		 #pragma omp critical
	   		{
	   			
	   			if (++cnt>next_step){
	   				
	   				printf("[%ld%%]\n",cnt/10737418 );
	   				
	   				
	   				next_step+=10737418;
	   			}
		   		//Bit libero -> segno 1
		   		if ((buckets[bucket_index]&bit_index)==0){
		       		buckets[bucket_index]|=bit_index;
		       		
				} 
				else{
				
					uint64_t* copied_hash = duplicate_hash(hash_key,9);
					
					HashInsert(ht, PTR_KEY(ht, copied_hash), i);
		        }
		 
	  		}

		}

	
	}
	printf("\nFreeing buckets...\n");
	free (buckets);
	printf("Searching for collisions...\n");
	cnt=0;
	HTItem* item = NULL;
	#pragma omp parallel  private (cube,cube2,item,hash_key) 
	{
		cube = alloc_cube();
		cube2 = alloc_cube();
		hash_key = malloc(80);
		#pragma omp for
		for(i=0;i<1073741824;i++){ 
		
			insert_cube_challenge_400(i,cube);

			three_rounds(cube,cube2,16);
			
			get_400_hash_from_cube(hash_key,cube);
			
			
			 item = HashFind(ht, PTR_KEY(ht, hash_key));
				if(item!=NULL && item->data!=i){
					fprintf(file,"[%ld,%ld]\n",item->data,i);
					fflush(file);
					
				}


	
		
		}

		free_cube(cube);
		free_cube(cube2);
		free(hash_key);
		
	}

	ClearHashTable(ht);
 	FreeHashTable(ht);
}


// void keccak_challenge_400_ridotta(){
// 	FILE* file = NULL;
// 	char* path = "400_ridotta.txt";

	
// 	struct HashTable* ht = AllocateHashTable(80, 0); 
	
// 	uint64_t i;
	
// 	file = fopen(path,"a");
// 	if (file==NULL){
// 		printf("\nFailed to open file.\n");
// 		return;
// 	}

// 	printf("\nStarting Attack.\n\n");
// 	uint64_t cnt=0;
// 	uint64_t cnt2=0;
// 	uint64_t next_step = 0;
// 	uint64_t** cube;
// 	uint64_t** cube2;
// 	uint64_t* hash_key;
// 	uint64_t* buckets = calloc((uint64_t)536870912,sizeof(uint64_t));

// 	if(buckets==NULL){
// 			fprintf(file,"Fatal: Out of memory.\n");
// 			return;
// 		}
	
// 	#pragma omp parallel private (cube,cube2,hash_key) 
// 	{
// 		cube = alloc_cube();
// 		cube2 = alloc_cube();
// 		hash_key = malloc(80);

// 		#pragma omp for
// 		for(i=0;i<14348907;i++){

			
// 			insert_cube_challenge_400_ridotta(i,cube);
// 			three_rounds(cube,cube2,16);
// 			theta_fast(cube,cube2,16);
// 			rho_fast(cube2,cube,16);
// 			pi_fast(cube,cube2);
// 			chi_fast(cube2,cube);
// 			cube[0][0]^=0x0000000000008000;

// 			get_400_hash_from_cube(hash_key,cube);
			
			
// 			uint64_t xxhash = XXH64 (hash_key, 80,0);
			
			
// 	        uint64_t bucket_index = (xxhash&0x00000007ffffffc0)>>6;
// 	   		uint64_t bit_index = (uint64_t)1<<(uint8_t)(xxhash&0x3F);


// 	   		 #pragma omp critical
// 	   		{
	   			
// 	   			if (++cnt>next_step){
	   				
// 	   				printf("[%ld%%]\n",cnt/143489 );
	   				
	   				
// 	   				next_step+=143489;
// 	   			}
// 		   		//Bit libero -> segno 1
// 		   		if ((buckets[bucket_index]&bit_index)==0){
// 		       		buckets[bucket_index]|=bit_index;
		       		
// 				} 
// 				else{
				
// 					uint64_t* copied_hash = duplicate_hash(hash_key,9);
					
// 					HashInsert(ht, PTR_KEY(ht, copied_hash), i);
// 		        }
		 
// 	  		}

// 		}

	
// 	}
// 	printf("\nFreeing buckets...\n");
// 	free (buckets);
// 	printf("Searching for collisions...\n");
// 	cnt=0;
// 	HTItem* item = NULL;
// 	#pragma omp parallel  private (cube,cube2,item,hash_key) 
// 	{
// 		cube = alloc_cube();
// 		cube2 = alloc_cube();
// 		hash_key = malloc(80);
// 		#pragma omp for
// 		for(i=0;i<14348907;i++){ 
		
// 			insert_cube_challenge_400_ridotta(i,cube);

// 			three_rounds(cube,cube2,16);
// 			theta_fast(cube,cube2,16);
// 			rho_fast(cube2,cube,16);
// 			pi_fast(cube,cube2);
// 			chi_fast(cube2,cube);
// 			cube[0][0]^=0x0000000000008000;
// 			get_400_hash_from_cube(hash_key,cube);
			
			
// 			 item = HashFind(ht, PTR_KEY(ht, hash_key));
// 				if(item!=NULL && item->data!=i){
// 					fprintf(file,"[%ld,%ld]\n",item->data,i);
					
// 				}


	
		
// 		}

// 		free_cube(cube);
// 		free_cube(cube2);
// 		free(hash_key);
		
// 	}

// 	ClearHashTable(ht);
//  	FreeHashTable(ht);
// }


















void keccak_challenge_800(){

	FILE* file = NULL;
    char* path = "800.txt";

	file = fopen(path,"a");
	if (file==NULL){
		printf("\nFailed to open file.\n");
		return;
	}

	initialize(5);
	struct HashTable* ht = AllocateHashTable(40, 0); 
	
	uint64_t i,cnt;

	cnt = 0;
	
	printf("\nStarting Attack.\n\n");
	uint64_t next_step = 0;
	uint64_t** cube;
	uint64_t** cube2;
	uint64_t* hash_key;
	
	cube = alloc_cube();
	cube2 = alloc_cube();
	hash_key = malloc(40);
	

	
	for(i=0;i<1048576;i++){	
		


		

		insert_cube_challenge_800(i,cube);
		 
		three_rounds(cube,cube2,32);
		 
		

		get_800_hash_from_cube(hash_key,cube);
		
		uint64_t* copied_hash = duplicate_hash(hash_key,4);
		
		HTItem* item = NULL;

		item = HashFind(ht, PTR_KEY(ht, hash_key));
		
		if(item!=NULL){
			cnt++;
			fprintf(file,"[%ld,%ld]\n",item->data,i);
		}
		else{
			//print_extended_hash(copied_hash,4);
			HashInsert(ht, PTR_KEY(ht, copied_hash), i);
		}
	}

	fprintf(file,"Total: %ld\n",cnt);
		
		


				
}


void keccak_challenge_1440(uint8_t part){
	int fraction = 0;
	
	//L'hash e' da 160 bit. 160/64 = 2,5 ~= 3 lane.
	//Una lane e' da 8 byte -> entry ht da 24
	struct HashTable* ht = AllocateHashTable(24, 0); 
	
	uint64_t i;
	FILE* file = NULL;
	char path[6];

	path[0]=part+48;
	path[1]='b';
	path[2]='.';
	path[3]='t';
	path[4]='x';
	path[5]='t';
	file = fopen(path,"a");
	if (file==NULL){
		fprintf(file,"\nFailed to open file.\n");
		return;
	}
	fprintf(file,"\nStarting Attack: Part %d.\n\n",fraction );
	uint64_t cnt=0;
	uint64_t cnt2=0;
	uint64_t next_step = 0;
	uint64_t** cube;
	uint64_t** cube2;
	uint64_t* hash_key;
	//2^31 parole -> 2^37 buckets -> 2^34 bytes -> 16 giga
	uint64_t* buckets = calloc((uint64_t)2147483648,sizeof(uint64_t));

	if(buckets==NULL)
	{
			fprintf(file,"Fatal: Out of memory.\n");
			return;
	}

	#pragma omp parallel private (cube,cube2,hash_key) 
	{
		cube = alloc_cube();
		cube2 = alloc_cube();
		hash_key = malloc(24);

		#pragma omp for
		for(i=0;i<94143178827;i++){
			#pragma omp critical
	   		{
				cnt++;
			}
		
			insert_cube_challenge_1440(i,cube);
		
			three_rounds(cube,cube2,64);
			
			if ((cube[0][0]&0x3) != fraction)
				continue;
		
			get_hash_from_1440_cube(hash_key,cube);
			
			uint64_t xxhash = XXH64 (hash_key, 24,0);
				
	        uint64_t bucket_index = (xxhash&0x0000001fffffffc0)>>6;
	   		uint64_t bit_index = (uint64_t)1<<(uint8_t)(xxhash&0x3F);


	   		#pragma omp critical
	   		{
	   			
	   			if (cnt>next_step){
	   				
	   				fprintf(file,"[%ld%%] ",cnt/94143178 );
	   				fflush(file);
	   				
	   				next_step+=94143178;
	   			}
		   		//Bit libero -> segno 1
		   		if ((buckets[bucket_index]&bit_index)==0)
		   		{
		       		buckets[bucket_index]|=bit_index;
				} 
				else
				{
					uint64_t* copied_hash = duplicate_hash(hash_key,2);
					
					HashInsert(ht, PTR_KEY(ht, copied_hash), i);
		        }
		
	  		}

		}
	
	}
	fprintf(file,"\nFreeing buckets...\n");
	free (buckets);
	fprintf(file,"Searching for collisions...\n");
	cnt=0;
	HTItem* item = NULL;
	#pragma omp parallel  private (cube,cube2,item,hash_key) 
	{
		cube = alloc_cube();
		cube2 = alloc_cube();
		hash_key = malloc(24);
		#pragma omp for
		for(i=0;i<94143178827;i++){ 
		
			insert_cube_challenge_1440(i,cube);
		
			three_rounds(cube,cube2,64);

			
			//controllo solo un quarto del subset
			if ((cube[0][0]&0x3) != fraction)
				continue;

			
			get_hash_from_1440_cube(hash_key,cube);
			
			
			item = HashFind(ht, PTR_KEY(ht, hash_key));
			if(item!=NULL && item->data!=i){
				fprintf(file,"[%ld,%ld]\n",item->data,i);
				
			}
		}

		free_cube(cube);
		free_cube(cube2);
		free(hash_key);
		
	}

	ClearHashTable(ht);
 	FreeHashTable(ht);
}


void set_lane(uint64_t n,uint64_t* lane){
	n&=0xF;
	*lane =  n |  n<<4 |  n<<8 |  n<<12 |  n<<16 |  n<<20 |  n<<24 |  n<<28 
	| n<<32 |  n<<36 |  n<<40 |  n<<44 |  n<<48 |  n<<52 |  n<<56 |  n<<60;
}




void three_rounds(uint64_t** cube,uint64_t** cube2, int w){
		 //print_hex_cube(cube);
		theta_fast(cube,cube2,w);
		rho_fast(cube2,cube,w);
		pi_fast(cube,cube2);
		chi_fast(cube2,cube);
		cube[0][0]^=0x0000000000000001;
		theta_fast(cube,cube2,w);
		rho_fast(cube2,cube,w);
		pi_fast(cube,cube2);
		// print_hex_cube(cube2);
		chi_fast(cube2,cube);
		cube[0][0]^=0x0000000000008082;
		theta_fast(cube,cube2,w);
		rho_fast(cube2,cube,w);
		pi_fast(cube,cube2);
		chi_fast(cube2,cube);
		if(w==64)
			cube[0][0]^=0x800000000000808a;
		else 
			cube[0][0]^=0x000000000000808a;
		// print_hex_cube(cube);


}

void one_dot_five_rounds(uint64_t** cube,uint64_t** cube2, int l){
		theta_fast(cube,cube2,l);
		rho_fast(cube2,cube,l);
		pi_fast(cube,cube2);
		chi_fast(cube2,cube);
		cube[0][0]^=0x0000000000000001;
		theta_fast(cube,cube2,l);
		rho_fast(cube2,cube,l);
		pi_fast(cube,cube2);


}

void insert_cube(uint64_t input_nr,uint64_t** cube){
		set_lane(input_nr%pows[0],&cube[0][0]);
		set_lane(input_nr/pows[0],&cube[1][0]);
		set_lane(input_nr/pows[1],&cube[2][0]);
		set_lane(input_nr/pows[2],&cube[3][0]);
		set_lane(input_nr/pows[3],&cube[4][0]);
		set_lane(input_nr/pows[4],&cube[0][1]);
		set_lane(input_nr/pows[5],&cube[1][1]);
		set_lane(input_nr/pows[6],&cube[2][1]);
		set_lane((input_nr/pows[7])|0xC,&cube[3][1]);
		cube[4][1]=0;
		uint8_t x,y;
		for (x=0;x<5;x++)
			for (y=2;y<5;y++)
				cube[x][y] = 0;
		
}

void insert_cube_challenge_800(uint64_t input_nr,uint64_t** cube){

		uint8_t x,y;
		for (y=0;y<4;y++){
			for (x=0;x<5;x++){
				if((1<<((5*y)+x)) & input_nr)
					cube[x][y]=0x0000000055555555;
				else 
					cube[x][y]=0x00000000FFFFFFFF;
			}
		}

		for (x=0;x<5;x++)
			cube[x][4]=0x0000000000000000;
		
}

void insert_cube_challenge_1440(uint64_t input_nr,uint64_t** cube){
		int mod;
		uint8_t x,y;
		for (y=0;y<5;y++){
			for (x=0;x<5;x++){
				if(y==4 && x==3) break;

				mod = input_nr%3;
				if (y==4 && x==2)
				{
					switch(mod){
					case 0: cube[x][y]=0x0000000000000000;break;
					case 1: cube[x][y]=0x0000000055555555;break;
					case 2: cube[x][y]=0x00000000AAAAAAAA;break;
					}
				}else{
					switch(mod){
					case 0: cube[x][y]=0x0000000000000000;break;
					case 1: cube[x][y]=0x5555555555555555;break;
					case 2: cube[x][y]=0xAAAAAAAAAAAAAAAA;break;
					}
				}
				input_nr/=3;
			}
		}

	cube[4][4]=0x0000000000000000;
	cube[3][4]=0x0000000000000000;

}

void insert_cube_challenge_400(uint64_t input_nr,uint64_t** cube){

		uint8_t x,y;
		for (y=0;y<3;y++){
			for (x=0;x<5;x++){
				int offset = (5*y+x)*2;
				int res = ((0x3 << offset) & input_nr) >> offset;
				if(res==0)
					cube[x][y]=0x0000000000000000;
				else if (res == 1)
					cube[x][y]=0x0000000000005555;
				else if (res == 2)
					cube[x][y]=0x000000000000AAAA;
				else if (res == 3)
					cube[x][y]=0x000000000000FFFF;
			}
		}

		for (x=0;x<5;x++){
			cube[x][4]=0x0000000000000000;
			cube[x][3]=0x0000000000000000;
		}
		
}

void insert_cube_challenge_400_ridotta(uint64_t input_nr,uint64_t** cube){

		int mod;
		uint8_t x,y;
		for (y=0;y<3;y++){
			for (x=0;x<5;x++){
				

				mod = input_nr%3;
			
					switch(mod){
					case 0: cube[x][y]=0x0000000000000000;break;
					case 1: cube[x][y]=0x0000000000005555;break;
					case 2: cube[x][y]=0x000000000000FFFF;break;
					}
				
				input_nr/=3;
			}
		}


		for (x=0;x<5;x++){
			cube[x][4]=0x0000000000000000;
			cube[x][3]=0x0000000000000000;
		}
		
}


void get_hash_from_cube(uint64_t *hash,uint64_t** cube){
		
		hash[0]=cube[0][0];
		hash[1]=cube[1][0];
		hash[2]=cube[2][0];
		hash[3]=cube[3][0];
		hash[4]=cube[4][0];
		hash[5]=cube[0][1];
		hash[6]=cube[1][1];
		hash[7]=cube[2][1];
}
	


void get_hash_from_1440_cube(uint64_t *hash,uint64_t** cube){
		
		hash[0]=cube[0][0];
		hash[1]=cube[1][0];
		hash[2]=cube[2][0]&0x00000000FFFFFFFF;

	
}
void get_400_hash_from_cube(uint64_t *hash,uint64_t** cube){
		
		hash[0]=cube[0][0];
		hash[1]=cube[1][0];
		hash[2]=cube[2][0];
		hash[3]=cube[3][0];
		hash[4]=cube[4][0];
		hash[5]=cube[0][1];
		hash[6]=cube[1][1];
		hash[7]=cube[2][1];
		hash[8]=cube[3][1];
		hash[9]=cube[4][1];
	
}
void get_800_hash_from_cube(uint64_t *hash,uint64_t** cube){
		
		hash[0]=cube[0][0];
		hash[1]=cube[1][0];
		hash[2]=cube[2][0];
		hash[3]=cube[3][0];
		hash[4]=cube[4][0];
	
}



uint64_t* duplicate_hash(uint64_t* old_hash, int l){
	uint64_t* new_hash = malloc(8*(l+1));
	uint8_t k;
	for(k=0;k<l+1;k++)
		new_hash[k]=old_hash[k];
	return new_hash;
}

void check_equals(uint64_t a, uint64_t b){
	uint64_t* h1 = malloc(64);
	uint64_t* h2 = malloc(64);
	uint64_t** c1 = alloc_cube();
	uint64_t** c2 = alloc_cube();
	uint64_t** c1_temp = alloc_cube();
	uint64_t** c2_temp = alloc_cube();

	insert_cube(a,c1);
	insert_cube(b,c2);
	//printf("\nA: %lu, B: %lu",a,b );
	//printf("\nA: ");
	//print_essential_cube(c1);
	//printf("B: ");
	//print_essential_cube(c2);
	three_rounds(c1,c1_temp,64);
	three_rounds(c2,c2_temp,64);
	get_hash_from_cube(h1,c1);
	get_hash_from_cube(h2,c2);
	uint8_t uguali = 1;
	uint8_t i;
	for(i=0;i<8;i++)
		if(h1[i]!=h2[i])
			uguali = 0;
	if(uguali==1){
		
		printf("Hash: ");
		print_extended_hash(h1,7);
	}else{
		printf("A e B non collidono.\n");
	}

	free_cube(c1);
	free_cube(c1_temp);
	free_cube(c2);
	free_cube(c2_temp);
	free(h1);
	free(h2);	
}

void check_equals_400(uint64_t a, uint64_t b){
	uint64_t* h1 = malloc(80);
	uint64_t* h2 = malloc(80);
	uint64_t** c1 = alloc_cube();
	uint64_t** c2 = alloc_cube();
	uint64_t** c1_temp = alloc_cube();
	uint64_t** c2_temp = alloc_cube();

	insert_cube_challenge_400(a,c1);
	insert_cube_challenge_400(b,c2);
	printf("\nA: %lu, B: %lu",a,b );
	printf("\nA: ");
	print_essential_cube(c1);
	printf("B: ");
	print_essential_cube(c2);
	three_rounds(c1,c1_temp,16);
	three_rounds(c2,c2_temp,16);
	get_400_hash_from_cube(h1,c1);
	get_400_hash_from_cube(h2,c2);
	uint8_t uguali = 1;
	uint8_t i;
	for(i=0;i<10;i++)
		if(h1[i]!=h2[i])
			uguali = 0;
	if(uguali==1){
		
		printf("Hash: ");
		print_extended_hash(h1,9);
	}else{
		printf("A e B non collidono.\n");
	}

	free_cube(c1);
	free_cube(c1_temp);
	free_cube(c2);
	free_cube(c2_temp);
	free(h1);
	free(h2);	
}

void check_equals_800(uint64_t a, uint64_t b){
	uint64_t* h1 = malloc(40);
	uint64_t* h2 = malloc(40);
	uint64_t** c1 = alloc_cube();
	uint64_t** c2 = alloc_cube();
	uint64_t** c1_temp = alloc_cube();
	uint64_t** c2_temp = alloc_cube();

	insert_cube_challenge_800(a,c1);
	insert_cube_challenge_800(b,c2);
	printf("\nA: %lu, B: %lu",a,b );
	printf("\nA: ");
	print_essential_cube(c1);
	printf("B: ");
	print_essential_cube(c2);
	three_rounds(c1,c1_temp,32);
	three_rounds(c2,c2_temp,32);
	get_800_hash_from_cube(h1,c1);
	get_800_hash_from_cube(h2,c2);
	uint8_t uguali = 1;
	uint8_t i;
	for(i=0;i<5;i++)
		if(h1[i]!=h2[i])
			uguali = 0;
	if(uguali==1){
		
		printf("Hash: ");
		print_extended_hash(h1,4);
	}else{
		printf("A e B non collidono.\n");
	}

	free_cube(c1);
	free_cube(c1_temp);
	free_cube(c2);
	free_cube(c2_temp);
	free(h1);
	free(h2);	
}

void print_essential_cube(uint64_t** cube){

	uint8_t x,y;
		for (y=0;y<2;y++){
				for (x=0;x<5;x++){
				if(y==1 && x==4)
					continue;
			
			printf("%016lx",cube[x][y]);
			if(y!=1 || x!=3) 
				printf("-");
		}
		
	}
				printf("\n");
}
