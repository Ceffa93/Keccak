#include "keccak_f_functions.h"

uint8_t rho_rotations[5][5] =   {{0,36,3,41,18},
								{1,44,10,45,2},
								{62,6,43,15,61},
								{28,55,25,21,56},
								{27,20,39,8,14}};
uint64_t** theta(uint64_t** from, uint8_t width){
	uint8_t x,y;
	uint64_t* temp_array_1 = calloc(5,64); 		
	uint64_t* temp_array_2 = calloc(5,64); 		
	uint64_t** to = alloc_cube();

	for (x=0;x<5;x++){
		temp_array_1[x]=from[x][0]; 
		for (y=1;y<5;y++){
			temp_array_1[x]^=from[x][y];
		}
	}


	for (x=0;x<5;x++){
		temp_array_2[x] = temp_array_1[(x+4)%5] ^ rot(temp_array_1[(x+1)%5],1,width);
		for (y=0;y<5;y++){
			to[x][y] = temp_array_2[x] ^ from[x][y];
		}
	}
	free(temp_array_1);
	free(temp_array_2);
	free_cube(from);
	return to;
}


uint64_t** rho(uint64_t** from,uint8_t width){
	uint8_t x,y,i,new_x,new_y;
	uint64_t** to = alloc_cube();
	
	to[0][0] = from[0][0];
	x=1;
	y=0;

	for (i = 0;i<24;i++){
	//	printf("[%d;%d] = :%d\n",x,y,((i+1)*(i+2)/2)%width);
		to[x][y]=rot(from[x][y],((i+1)*(i+2)/2)%width, width);
		new_x = y;
		new_y = (2*x + 3*y)%5;
		x = new_x;
		y = new_y;
	}
	free_cube(from);
	return to;
}

uint64_t** pi(uint64_t** from){
	uint8_t x,y;
	uint64_t** to = alloc_cube();
	for (x=0;x<5;x++){
		for(y=0;y<5;y++){
			to[y][(2*x+3*y)%5]=from[x][y];
		}
	}
free_cube(from);
	return to;
}



uint64_t** chi(uint64_t** from){
	uint8_t x,y;
	uint64_t** to = alloc_cube();
	for (x=0;x<5;x++){
		for(y=0;y<5;y++){
			to[x][y]=from[x][y]^((~from[(x+1)%5][y]) & from[(x+2)%5][y]);
		}
	}
	free_cube(from);
	return to;
}

uint64_t ** iota(uint64_t** from,uint64_t* rc, uint8_t current_round){
	//copia inutilmente pesante del cubo, ma puo' essere utile avere il cubo prima e dopo il round
	uint64_t** to = alloc_cube();
	uint8_t x,y;
	for (x=0;x<5;x++){
		for(y=0;y<5;y++){
			to[x][y]=from[x][y];
		}
	}

	to[0][0] = from[0][0]^rc[current_round];
	free_cube(from);
	return to;

}




void theta_fast(uint64_t** from,uint64_t** to, uint8_t width){
	uint8_t x,y;
	uint64_t* temp_array_1 = calloc(5,64); 		
	uint64_t* temp_array_2 = calloc(5,64); 		
	

	for (x=0;x<5;x++){
		temp_array_1[x]=from[x][0]; 
		for (y=1;y<5;y++){
			temp_array_1[x]^=from[x][y];
		}
	}


	for (x=0;x<5;x++){
		temp_array_2[x] = temp_array_1[(x+4)%5] ^ rot(temp_array_1[(x+1)%5],1,width);
		for (y=0;y<5;y++){
			to[x][y] = temp_array_2[x] ^ from[x][y];
		}
	}
	free(temp_array_1);
	free(temp_array_2);
	
}


void rho_fast(uint64_t** from,uint64_t** to,uint8_t width){
	uint8_t x,y,i,new_x,new_y;
	
	
	to[0][0] = from[0][0];
	x=1;
	y=0;

	for (i = 0;i<24;i++){
	//	printf("[%d;%d] = :%d\n",x,y,((i+1)*(i+2)/2)%width);
		to[x][y]=rot(from[x][y],((i+1)*(i+2)/2)%width, width);
		new_x = y;
		new_y = (2*x + 3*y)%5;
		x = new_x;
		y = new_y;
	}
	
}

void pi_fast(uint64_t** from,uint64_t** to){
	uint8_t x,y;
	
	for (x=0;x<5;x++){
		for(y=0;y<5;y++){
			to[y][(2*x+3*y)%5]=from[x][y];
		}
	}

}



void chi_fast(uint64_t** from,uint64_t** to){
	uint8_t x,y;
	for (x=0;x<5;x++){
		for(y=0;y<5;y++){
			to[x][y]=from[x][y]^((~from[(x+1)%5][y]) & from[(x+2)%5][y]);
		}
	}

}


void iota_fast(uint64_t** from,uint64_t rc){
	
	from[0][0]^=rc;

}
