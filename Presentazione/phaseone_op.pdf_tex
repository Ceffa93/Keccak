%% Creator: Inkscape inkscape 0.48.4, www.inkscape.org
%% PDF/EPS/PS + LaTeX output extension by Johan Engelen, 2010
%% Accompanies image file 'phaseone_op.pdf' (pdf, eps, ps)
%%
%% To include the image in your LaTeX document, write
%%   \input{<filename>.pdf_tex}
%%  instead of
%%   \includegraphics{<filename>.pdf}
%% To scale the image, write
%%   \def\svgwidth{<desired width>}
%%   \input{<filename>.pdf_tex}
%%  instead of
%%   \includegraphics[width=<desired width>]{<filename>.pdf}
%%
%% Images with a different path to the parent latex file can
%% be accessed with the `import' package (which may need to be
%% installed) using
%%   \usepackage{import}
%% in the preamble, and then including the image with
%%   \import{<path to file>}{<filename>.pdf_tex}
%% Alternatively, one can specify
%%   \graphicspath{{<path to file>/}}
%% 
%% For more information, please see info/svg-inkscape on CTAN:
%%   http://tug.ctan.org/tex-archive/info/svg-inkscape
%%
\begingroup%
  \makeatletter%
  \providecommand\color[2][]{%
    \errmessage{(Inkscape) Color is used for the text in Inkscape, but the package 'color.sty' is not loaded}%
    \renewcommand\color[2][]{}%
  }%
  \providecommand\transparent[1]{%
    \errmessage{(Inkscape) Transparency is used (non-zero) for the text in Inkscape, but the package 'transparent.sty' is not loaded}%
    \renewcommand\transparent[1]{}%
  }%
  \providecommand\rotatebox[2]{#2}%
  \ifx\svgwidth\undefined%
    \setlength{\unitlength}{2105.58007812bp}%
    \ifx\svgscale\undefined%
      \relax%
    \else%
      \setlength{\unitlength}{\unitlength * \real{\svgscale}}%
    \fi%
  \else%
    \setlength{\unitlength}{\svgwidth}%
  \fi%
  \global\let\svgwidth\undefined%
  \global\let\svgscale\undefined%
  \makeatother%
  \begin{picture}(1,0.39006374)%
    \put(0,0){\includegraphics[width=\unitlength]{phaseone_op.pdf}}%
    \put(0.00021817,0.35023178){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$2^{34}$
}}}%
    \put(0.07994137,0.35023178){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$2^{31}$
}}}%
    \put(0.0008467,0.3753916){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{\scriptsize\textbf{Phase 1}
}}}%
    \put(0.16832665,0.33279602){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{Hash function}}}%
    \put(0.3242954,0.24798087){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{A[h/8]}}}%
    \put(0.25220743,0.27096962){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{4GB Array}}}%
    \put(0.4368414,0.10000451){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{Hash table}}}%
    \put(0.18587837,0.05487673){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{Table entry}}}%
    \put(0.18861013,0.0331462){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{Key}}}%
    \put(0.189278,0.00928006){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{Data}}}%
    \put(0.17539718,0.30530692){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$2^{512} \rightarrow 2^{35}$}}}%
    \put(0.42491966,0.13755309){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{If bit = 0}}}%
    \put(0.42442653,0.18708851){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{Set bit = 1}}}%
    \put(0.30370877,0.02257557){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{Add entry}}}%
    \put(0.21214491,0.20354178){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{Get bit [h mod 8] }}}%
    \put(0.34925309,0.29012509){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$h$}}}%
    \put(-0.00010686,0.33319361){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{Inputs}}}%
    \put(0.07958912,0.33293533){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{Outputs}}}%
    \put(0.30616006,0.06817862){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{If bit = 1}}}%
    \put(0.00369256,0.08338143){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{Get $2^{34}$ bit index}}}%
    \put(0.5289026,0.34795604){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$2^{34}$
}}}%
    \put(0.60862589,0.34795604){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{$2^{31}$
}}}%
    \put(0.52953116,0.37526498){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{\scriptsize\textbf{Phase 2}
}}}%
    \put(0.754252,0.29223856){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{Hash table}}}%
    \put(0.85121452,0.26841364){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{Table entry}}}%
    \put(0.85394629,0.24668305){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{Key}}}%
    \put(0.85461415,0.22281691){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{Data}}}%
    \put(0.67578027,0.24595819){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{Search}}}%
    \put(0.52857759,0.33091787){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{Inputs}}}%
    \put(0.60827363,0.33065959){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{Outputs}}}%
    \put(0.53237702,0.13559758){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{Get $2^{34}$ bit index}}}%
    \put(0.83264431,0.13642093){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{If not equals}}}%
    \put(0.830813,0.10196979){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{\textbf{Collision found}}}}%
    \put(0.6757776,0.228371){\color[rgb]{0,0,0}\makebox(0,0)[lb]{\smash{for key}}}%
  \end{picture}%
\endgroup%
