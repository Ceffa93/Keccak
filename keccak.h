#ifndef KECCAK_H
#define KECCAK_H


#include <QWidget>
#include <QFile>
#include <QTextStream>
#include <QTextEdit>

namespace Ui {
class Keccak;
}

class Keccak : public QWidget
{
    Q_OBJECT

public:
    explicit Keccak(QWidget *parent = 0);
    ~Keccak();


private slots:

    void on_generateButton_clicked();

    void on_binaryRadio_clicked();

    void on_asciiRadio_clicked();

private:
    Ui::Keccak *ui;
    QFile inputFile;
    QFile outputFiles[11];

    void setupFiles();
    void loadOutputFiles();
    void loadFile(u_int8_t  i, QTextEdit* t);
    void write_on_input();

};

#endif // KECCAK_H
